<?php

require_once __DIR__.'/../app/bootstrap.php';

$app = require __DIR__.'/../app/app.php';
require_once __DIR__.'/../app/routing.php';

$app->register(new Predis\Silex\ClientServiceProvider(), array(
    'predis.parameters' => 'tcp://127.0.0.1:6379/'
));

$app->run();