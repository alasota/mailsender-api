<?php

/**
 * @author Artur Lasota <lasota.artur@gmail.com>
 */

namespace Mail\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MailController
{
    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function sendAction(Request $request, Application $app)
    {
        $data = array();
        $data['to'] = $request->get('to');
        $data['cc'] = $request->get('cc');
        $data['subject'] = $request->get('subject');
        $data['message'] = $request->get('message');
        $data['uniq_hash'] = md5(uniqid()); //uniq data

        $app['predis']->lpush('mail-queue', serialize($data));

        $response = new JsonResponse();
        return $response->setData(array('success' => true));
    }

} 
