<?php

/**
 * @author Artur Lasota <lasota.artur@gmail.com>
 */

namespace Mail\Controller;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController
{
    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function helloAction(Request $request, Application $app)
    {
        $response = new Response();
        $response->setContent("Hello world2");
        return $response;
    }
} 